![picture](child-in-crowd-static/img/ChildInCrowdLogo.png)

## What it does (one phrase):
Protects children from kidnappers in a crowded event using artificial intelligence

## About:
Child in Crowd is an effort to protect children from kidnappers that take advantage of crowds to haunt their victims. We bring an intelligent tool that link children with their relatives from single image used for check in, letting them prove their relationship when checking out using a second image.

### How does it work?
Child in Crowd extracts the biometrics from people faces and relate those unique face characteristics to form groups; the algorithm first detects children on an image and for builds the groups. As faces are signed with unique biometric signatures, another picture of the face will be recognized. Child in Crowd is powered by AWS machine learning.

### Child in Crowd parents compromise:
> The implementation of Child in Crowd service in a crowded event requires a parents compromise to take an image at the entrance and another image at the exit of the event. Child in Crowd will only work if all the parents in the event take the picture at the entrance. By these means we can check the children link to their relatives and suspect of an individual refusing to do it at the exit. We parents, should fight together against children kidnappers.

## Documentation:
Child in Crowd service is composed of two parts: an online interface that can be accessed here, and a processing unit in the cloud brought as an AWS Serverless App repository. The online interface can be used by anyone without any cost while the computing costs in AWS will be covered by the deployer. Depending on your usage, Child in Crowd computing costs could result free when registering hundreds of users. Below, you can find information related on how to use Child in Crowd technology in your events and protect children from abusers in the crowd.

###The online interface:
You can access the online interface in the link below:
[child in Crowd Online Interface](https://childincrowd.org/reception-app.html)

As you can notice, the online interface asks you for:
An endpoint: that is the URL at which your own Child in Crowd processing unit is served (provided after deploying the Crowd in Child AWS Serverless App)
An API key: used for security so only allowed browsers will be able to access your processing unit (also (provided after deploying the Crowd in Child AWS Serverless App)

Once you entered your endpoint and API key, you will be prompted to access your webcam; when accepting it, you will see video casted from it. Only one button is needed to use Child in Care online interface (at least for checking in and out). 

*To check-in:*
In order to check-in you will need to make a picture of the child with his relatives. Child in Crowd processing unit will recognize all the children's faces in the picture and relate them with the adults in the picture. This process can take up to 6 seconds.

*To check-out:*
Each time a new image is taken on the Child in Crowd interface, the processing unit will check if the children faces are already registered with their relatives and if so, it will check if at least one adult in the new picture coincides with relatives registered in the later check-in image. If none of the adults coincides with the check-in relatives the interface will throw a warning supplying the check-in image for the child where the the relatives were not found. 

In some cases, it is possible that the processing unit will not recognize faces in the picture. The online interface will guide on how to take a good picture in order to avoid errors and delays in your event entrance. It is recommended that the webcam will take well balanced images (not too bright and with good contrast), that all the faces in the pictures will be independent (not covering each other) and that the people shoulders will fit on the webcam's frame.

###The processing unit:
Child in Crowd processing unit is offered as an AWS Serverless App for free at the [AWS Serverless App repository](https://serverlessrepo.aws.amazon.com/applications/arn:aws:serverlessrepo:us-east-1:256572818498:applications~ChildInCrowd). By these means, the technology is provided for free to those who want to implement children security on their crowded places. AWS costs could be applied if the free tiers are exceeded; normally, you will be able to register hundreds of children faces without any related costs.

To deploy a new Child in Crowd processing unit instant, you need to create an Amazon AWS account (if you already not have one) and access the AWS repository. Here you can find Child in Crowd processing unit deployment page. After clicking deploy wait for the deployment and copy both your URL endpoints and the API key that will be used in the Child in Crowd online interface.

##Child in Crowd implementation recommendations:
Child in Crowd is provided as a tool for everyone to make easy to implement children security on events. It should be considered an extra security layer that human guards can use to avoid children kidnapping.

We recommend to use a computer with a webcam or a tablet at each access point of your entrance and carefully handle your endpoint URL and API key accees credentials. It is recommendable that someone at the entrance will look for Child in Crowd messages and ensure that children will get registered. "Child in Crowd" parents compromise should be explained in order to fight against kidnappers.

If you are an events organizer, we strongly recommend to deploy a new Child in Crowd processing unit instance from the AWS Serverless repository for each event that you organize.

##Future work:
Our goal is to keep children safe from human trafficking, for this reason new functionalities are being developed for rescuing children using the Child in Crowd platform.
