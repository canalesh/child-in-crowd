'use strict';

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
var docClient = new AWS.DynamoDB.DocumentClient();
var table = process.env.DYNAMO_DB;

// var params = {
//     TableName: table,
//     Key: {
//         userId: this.event.context.System.user.userId
//     } 
// };
// console.log("Reading a new item to DynamoDB...");
// let databaseResponse = await docClient.get(params).promise()

function addChildToDatabase (childId, relativesIds, imageS3Key)  {  
    var params = {
        TableName: table,
        Item:{
            "id": childId,
            "data": {
                checkinS3Key: imageS3Key,
                relativesIds: relativesIds
            }
        }
    };
    console.log("Adding a new item to DynamoDB...", params);
    return docClient.put(params).promise()
} 

function getChildFromDatabase (childId)  {  
    var params = {
        TableName: table,
        Key: {
            id: childId
        } 
    };
    console.log("Reading a new item to DynamoDB...");
    return docClient.get(params).promise()
} 

function deleteChildFromDatabase (childId)  {  
    var params = {
        TableName: table,
        Key: {
            id: childId
        } 
    };
    console.log("Deleting a item from DynamoDB...");
    return docClient.delete(params).promise()
} 

module.exports.addChildToDatabase = addChildToDatabase;  
module.exports.deleteChildFromDatabase = deleteChildFromDatabase;  
module.exports.getChildFromDatabase = getChildFromDatabase;


