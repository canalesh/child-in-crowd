'use strict';

var databaseHelper = require('./databaseHelper')

const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});

const rekognition = new AWS.Rekognition({ apiVersion: '2016-06-27' });
const S3 = new AWS.S3();

const Jimp = require('jimp')
const uuidv1 = require('uuid/v1');

class ChildrenCheckinCheckout {

  // 1 - I need to ingest an image and search children faces from the database
  // 2 - If the child face is not found, index faces is called and added to the database
  // 3 - If the child face is recognized, the relatives are compared and if at least one is retrieved, the child is ok

  constructor (s3Config) {
    this.imageName = s3Config.imageName
  }

  searchFacesById(s3Config, faceId) {
    var params = {
      CollectionId: process.env.FACES_COLLECTION, 
      FaceId: faceId, 
      FaceMatchThreshold: 90, 
      MaxFaces: 1
     };
     return rekognition.searchFaces(params).promise() // data.FaceMatches
  }

  async searchFacesOnImages(detectedFaces) {
    try {
      for (let idx=0; idx < detectedFaces.length; idx++)  {
        // console.log('TO test, upload the face that is being tested: ', detectedFaces[idx].Blob)
        // await this.putS3Object(this.imageName +'childBlob.jpg', detectedFaces[idx].Blob)
        var params = {
          CollectionId: process.env.FACES_COLLECTION,
          FaceMatchThreshold: 75, 
          Image: {
            Bytes: detectedFaces[idx].Blob
          }, 
          MaxFaces: 1
         };
         let faceId = await rekognition.searchFacesByImage(params).promise() // data.FaceMatches
         // console.log('Here the response from search faces: ', faceId)
         if (faceId.FaceMatches.length > 0) {
          detectedFaces[idx].faceId = faceId.FaceMatches[0].Face.FaceId || undefined
          detectedFaces[idx].new = false // Here we determine if the face is new in the collection
         } else {
          detectedFaces[idx].new = true
         }
      }
      return detectedFaces
    } catch (error) {
      console.log('An error occurred when searching faces: ', error)
      return error
    }    
  }

  async indexNewFaces(detectedFaces) {
    try {
      for (let idx=0; idx < detectedFaces.length; idx++)  {
        // console.log('Detected Faces condition in index: ', detectedFaces[idx].new)
        if (detectedFaces[idx].new) { // Here we only index new faces
          var params = {
            CollectionId: process.env.FACES_COLLECTION,
            ExternalImageId: this.imageName,
            MaxFaces: 1,
            DetectionAttributes: [
            ], 
            Image: {
              Bytes: detectedFaces[idx].Blob
            }
           };
           let faceId = await rekognition.indexFaces(params).promise() // data.FaceRecords
           // console.log('Here the response from index faces: ', faceId)
           detectedFaces[idx].faceId = faceId.FaceRecords[0].Face.FaceId || undefined
        }
      }
      return detectedFaces
    } catch (error) {
      console.log('An error occurred when indexing faces: ', error)
      return error
    }    
  }

  async annotateFaces() {
    try {
      var params = {
        Image: {
          S3Object: {
            Bucket: process.env.S3_BUCKET, 
            Name: this.imageName,
          }
        },
        Attributes: [
          "ALL",
        ]
      };
      // Here I get the face details (interested in the AgeRange)
      let facesDetails = await rekognition.detectFaces(params).promise() // data.FaceDetails
      let detectedFaces = []
      let childNumber = 0
      // console.log('Raw Detected faces: ', facesDetails.FaceDetails)
      facesDetails.FaceDetails.map(val => {
        if (val.AgeRange.Low <= 15){
          // Here I get the child information
          childNumber += 1
          detectedFaces.push({BoundingBox: val.BoundingBox,
                              checkInImageUrl: null,
                              childNumber: childNumber,
                              message: null,
                              checkinS3Key: null,
                              Blob: null,
                              child: true, 
                              faceId: undefined,
                              new: undefined })
        } else {
          // Here the relatives information
          detectedFaces.push({BoundingBox: val.BoundingBox,
                              Blob: null,
                              child: false, 
                              faceId: undefined,
                              new: undefined })
        }
      })
      return detectedFaces
    } catch (error) {
      console.log('An error occured when detecting faces: ', error)
    }
  }

  signedS3GetUrl(objectKey) { 
    var params = {
      Bucket: process.env.S3_BUCKET,
      Key: objectKey
    }
    return S3.getSignedUrl('getObject', params)
  }

  putS3Object(key, body) {
    var params = {
      Body: body,
      Bucket: process.env.S3_BUCKET,
      ContentType: 'image/jpeg',
      Key: key
    }
    let putPromise = S3.putObject(params).promise()
    return putPromise.then(res => this.signedS3GetUrl(key))
  }

  getS3Object() {
    return S3.getObject({
      Bucket: process.env.S3_BUCKET,
      Key: this.imageName,
      ResponseContentType: 'image/jpeg'
    }).promise()
  }

  async getCroppedImageBlobs (detectedFaces, image) {
    // Here we get the cropeed image
    try {
      for (let idx=0; idx < detectedFaces.length; idx++)  {
        let BoundingBox = detectedFaces[idx].BoundingBox
        // console.log('Here the image: ', image, typeof image, image.Body.buffer)
        let jimpImageObject = await Jimp.read(image.Body.buffer)
        let imageWidth = jimpImageObject.bitmap.width;
        let imageHeight = jimpImageObject.bitmap.height;
        let imageGap = 0.075 * ((imageHeight + imageWidth) / 2)
        // console.log('Here the Bounding Box: ', BoundingBox, typeof BoundingBox.Top, imageHeight*BoundingBox.Top)
        jimpImageObject.crop(imageWidth*BoundingBox.Left - 0.5*imageGap, imageHeight*BoundingBox.Top - 0.5*imageGap, imageWidth*BoundingBox.Width + imageGap, imageHeight*BoundingBox.Height + imageGap)
        jimpImageObject.normalize() // Here we add a normalization for better image quality
        let imageBlob = await jimpImageObject.getBufferAsync(Jimp.MIME_JPEG) // This is a promise
        // let savedImage = await jimpImageObject.writeAsync(`C:\Users\horac\Documents\OFFLINE_CODING\child-in-crowd\child-crowd-care-back\imagetest_${idx}_${detectedFaces[idx].faceId}.jpg`); // This is a promise
        detectedFaces[idx].Blob = imageBlob
        // console.log('Here a new Image with blob: ', detectedFaces[idx].Blob)
      }
      return detectedFaces
    } catch (error) {
      console.log('An error occurred when cropping an image: ', error)
      return error
    }
  }

  async addNewChildren (detectedFaces) {
    try {
      let relatives = detectedFaces.filter(val => !val.child) // Here we get the children relatives
      if (detectedFaces.filter(val => val.child && val.new).length !== 0) { // We only do it if there are new children
        for (let idx=0; idx < detectedFaces.length; idx++)  {
          if (detectedFaces[idx].child && detectedFaces[idx].new) { // Here only is the child is new
            let databaseResponse = await databaseHelper.addChildToDatabase(detectedFaces[idx].faceId, 
                                                                           relatives.map(val => val.faceId),
                                                                           this.imageName)
            // console.log('Here what the database returns: ', databaseResponse)
            detectedFaces[idx].message = 'Welcome!' // Here we add the welcoming message
          }
        }
      }
      // Item:
      // { id: '53e3c54d-bf64-4bae-8967-e02a8629ccbb',
      //   data: { relativesIds: [Array] } } }
      return detectedFaces
    } catch (error) {
      console.log('An error occurred adding children to database: ', error)
      return error
    }
  }

  async checkChildrenRelatives (detectedFaces) {
    try {
      let candidateRelatives = detectedFaces.filter(val => !val.child) // Here we get the children relatives
      if (detectedFaces.filter(val => val.child && !val.new).length !== 0) { // We only children that were already registered
        for (let idx=0; idx < detectedFaces.length; idx++)  {
          if (detectedFaces[idx].child && !detectedFaces[idx].new) { // Here only if the child was already registered
            let databaseResponse = await databaseHelper.getChildFromDatabase(detectedFaces[idx].faceId)
            // console.log('Here what the database returns when getting a child: ', databaseResponse)
            if (Object.keys(databaseResponse).length !== 0) {
              let childRelatives = databaseResponse.Item.data.relativesIds 
              let checkinS3Key = databaseResponse.Item.data.checkinS3Key 
              let isRelativePresent = candidateRelatives.map(val => val.faceId).some(val => childRelatives.indexOf(val) >= 0)
              // console.log('Here the response for isRelative Present: ', isRelativePresent, childRelatives)
              detectedFaces[idx].checkinS3Key = checkinS3Key
              if (isRelativePresent) {
                detectedFaces[idx].message = 'Good Bye!' // Here we add the good bye message
              } else {
                // console.log('Here we check the s# key for ckein image: ', checkinS3Key)
                detectedFaces[idx].message = 'Warning!' // Here we add the warning message
                detectedFaces[idx].checkInImageUrl = await this.signedS3GetUrl(checkinS3Key)
              }
            }
          }
        }
      }
      return detectedFaces
    } catch (error) {
      console.log('An error occurred adding children to database: ', error)
      return error
    }
  }

  async deleteFacesHandler(detectedFaces) {
    try {
      let children = detectedFaces.filter(val => val.child)
      let allCheckingOut = children.every(val => val.message === 'Good Bye!' || val.message === 'Warning!')
      if (allCheckingOut) { // Here if all the children are Welcome! or Warning! we delete all the faces
        
        for (let idx=0; idx < children.length; idx++)  {
          // Here we need to delete both from the collection and database
          let atomicResponse = await Promise.all([rekognition.deleteFaces({ CollectionId: process.env.FACES_COLLECTION, FaceIds: [children[idx].faceId] }).promise(), 
                                                  databaseHelper.deleteChildFromDatabase(children[idx].faceId)])
          console.log("Here the atomic delete response: ", atomicResponse);
        }
      } 
      return detectedFaces
    } catch (error) {
      console.log('An error occurred deleting the children faces: ', error)
    }
  }

  async drawSelectiveBoundingBoxForChildren (s3Image, detectedFaces) {
    // In this function we draw a bounding box around the children faces on the s3Image
    let lineThickness = 5
    let image = await Jimp.read(s3Image.Body.buffer)
    let imageWidth = image.bitmap.width;
    let imageHeight = image.bitmap.height;
    for (let idx=0; idx < detectedFaces.length; idx++)  {
      if (detectedFaces[idx].child) {
        let xBound = imageWidth * detectedFaces[idx].BoundingBox.Left
        let yBound = imageHeight * detectedFaces[idx].BoundingBox.Top
        let wBound = imageWidth * detectedFaces[idx].BoundingBox.Width
        let hBound = imageHeight * detectedFaces[idx].BoundingBox.Height
        let boundColorHex = null
        switch (detectedFaces[idx].message) {
          case 'Welcome!':
            boundColorHex = Jimp.rgbaToInt(0, 0, 255, 0.5)
            console.log('Selecting blue', boundColorHex)
            break;
          case 'Warning!':
            boundColorHex = Jimp.rgbaToInt(255, 255, 0, 0.5)
            console.log('Selecting yellow', boundColorHex)
            break;
          case 'Good Bye!':
            boundColorHex = Jimp.rgbaToInt(0, 255, 0, 0.5)
            console.log('Selecting green', boundColorHex)
            break;
          default:
            break;
        }
        // Bottom line
        image.scan(xBound, yBound, wBound + lineThickness, lineThickness, function (x, y, pidx) {
          // console.log('Here some scan: ', x, y, pidx, boundColorHex, typeof x, typeof y, typeof boundColorHex)
          image.setPixelColor(boundColorHex, x, y); // sets the colour of that pixel
        })
        // Top line
        image.scan(xBound, yBound + hBound, wBound, lineThickness, function (x, y, pidx) {
          image.setPixelColor(boundColorHex, x, y); // sets the colour of that pixel
        })
        // Left line
        image.scan(xBound, yBound, lineThickness, hBound, function (x, y, pidx) {
          image.setPixelColor(boundColorHex, x, y); // sets the colour of that pixel
        })
        // Right line
        image.scan(xBound + wBound, yBound, lineThickness, hBound, function (x, y, pidx) {
          image.setPixelColor(boundColorHex, x, y); // sets the colour of that pixel
        })
        // Here the child number
        await Jimp.loadFont(Jimp.FONT_SANS_64_WHITE).then(font => {
          console.log('Writing the child: ', detectedFaces[idx].childNumber)
          image.print(font, xBound + 2*lineThickness, yBound + 2*lineThickness, detectedFaces[idx].childNumber);
        })
      }
    }
    // We save the image for testing purposes
    // let savedImage = await image.writeAsync('C:\Users\horac\Documents\OFFLINE_CODING\child-crowd-care\child-crowd-care-back\imageBoundTest.jpg') // This is a promise
    return image.getBufferAsync(Jimp.MIME_JPEG) // This is a promise
  }

  async admission() {
    try {
      // First we need to make sure that we have the collection created
      // FIXME: This is an unhappy way of solving the ListCollections operation that is not allowed in AWS SAM. Maybe better in the future
      try {
        var paramsCollection = {
          CollectionId: process.env.FACES_COLLECTION
        };
        await rekognition.createCollection(paramsCollection).promise()
      } catch (err) {
        if (err.code === 'ResourceAlreadyExistsException') {
          console.log(err.message, '... We continue execution...')
        } else {
          console.log('An error ocurred when creating a collection, ', err)
        }
      }

      // We call the the detect faces from image operation. 
      let detectedFaces = await this.annotateFaces()
      // The first task will be to recognize if there is a child in the image (only one child per image support)
      if (detectedFaces.filter(val => val.child).length === 0) {
        return {imageUrl: null, children: null, errorMessage: 'No children found in the image. Please try again.'}
      } 
      if (detectedFaces.filter(val => !val.child).length === 0) {
        return { imageUrl: null, children: null, errorMessage: 'No adults found in the image. Please try again.' }
      } 

      // Now I need to crop the child image (to see if the child is already in the collection)
      let s3Image = await this.getS3Object()
      detectedFaces = await this.getCroppedImageBlobs(detectedFaces, s3Image)
      // console.log('Here the images Blobs example: ', detectedFaces.map(val => val.Blob))

      // Now we check if the faces are already in the faces collection
      detectedFaces = await this.searchFacesOnImages(detectedFaces)
      if (!Array.isArray(detectedFaces)) {
        return {imageUrl: null, children: null, errorMessage: 'Please take the picture again. Try to do show your complete faces'}
      }
      console.log('Here the detectedFaces: ', detectedFaces, typeof detectedFaces)
      console.log('Here the images faces ids example: ', detectedFaces.map(val => val.faceId))

      // We set the users to new if not registered yet. Done in the promise of searchFacesOnImages
      console.log('Here the images that are new: ', detectedFaces.map(val => val.new))

      // Now we index all faces that are new
      detectedFaces = await this.indexNewFaces(detectedFaces)
      console.log('Here the images faces ids after indexing: ', detectedFaces.map(val => val.faceId))

      // Now we save the child to the database if the child is new
      detectedFaces = await this.addNewChildren(detectedFaces)
      
      // If not new, we delete the child from the database and from the collection (from the collection we delete everyone)
      detectedFaces = await this.checkChildrenRelatives(detectedFaces)
      let children = detectedFaces.filter(val => val.child) // We declare the children with their messages

      // Here we will check if there are children both cheking out and cheking it. If so we take no action and roll back
      let messages = children.map(val => val.message)
      if ((messages.indexOf('Good Bye!') >= 0 && messages.indexOf('Welcome!') >= 0) || (messages.indexOf('Warning!') >= 0 && messages.indexOf('Welcome!') >= 0)) {
        await this.deleteFacesHandler(children.filter(val => val.message === 'Welcome!'))
        return {imageUrl: null, children: null, errorMessage: 'Please make the children pictures independently.'}
      }

      // Now we need to clean the collection if all the children have their parents
      // If there is a child with Welcome! we leave the parents but still delete the children
      detectedFaces = await this.deleteFacesHandler(detectedFaces)

      // Now we draw a bounding box around the faces (on the main image)
      s3Image = await this.drawSelectiveBoundingBoxForChildren(s3Image, detectedFaces)
      // Now we upload and get a pre signed link to get from the front-end
      let signedRegisterImage = await this.putS3Object(this.imageName.split('.jpg')[0] + '_registry.jpg', s3Image)
      // console.log('Here the signed URL for the registry image: ', signedRegisterImage, this.imageName + '_registry')
      // console.log('Here the faces checked: ', detectedFaces.filter(val => val.child))

      // Here we clean the information that will be sent to the user
      children.map(val => {
        delete val['BoundingBox']
        delete val['checkinS3Key']
        delete val['Blob']
        delete val['faceId']
        delete val['new']
        delete val['child']
      })

      const response = {imageUrl: signedRegisterImage, children: children}
      console.log('Here the response: ', response)
      return response

    } catch (error) {
      console.log('An error occurred during the child admission: ', error)
      return error
    }
  }
}

// const childrenCheckinCheckout = new ChildrenCheckinCheckout({imageName: 'grand.jpg'})

// // var params = {
// //   CollectionId: process.env.FACES_COLLECTION
// // } 
// // rekognition.deleteCollection(params, function(err, data) {
// //   if (err) console.log(err, err.stack); // an error occurred
// //   else     console.log(data);
// // });

// // var params = {
// //   CollectionId: process.env.FACES_COLLECTION
// // } 
// // rekognition.listFaces(params, function(err, data) {
// //   if (err) console.log(err, err.stack); // an error occurred
// //   else     console.log(data);
// // });

// childrenCheckinCheckout.admission()

module.exports = ChildrenCheckinCheckout;