'use strict';

var getURL  = require('./helpers/getPostSignedURL');
const ChildrenCheckinCheckout = require('./helpers/childrenCheckinCheckout');

module.exports.getSignedURL = async (event, context) => {
  // Here first I should get a signedS3 URL to upload the image
  // Then I need to upload the image from the client
  // Then I need to send the file name and perform the face rekognition
  const signedS3Url = await getURL.generatePresignedURL(event.body)
  console.log('Here the created signed url: ', signedS3Url)
  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin" : "*"
    },
    body: JSON.stringify({
        input: event,
        singedS3Url: signedS3Url
    }),
  };
};

module.exports.checkChild = async (event, context) => {
  // Here first I should get a signedS3 URL to upload the image
  // Then I need to upload the image from the client
  // Then I need to send the file name and perform the face rekognition
  console.log('Here the ingested event data: ', event)
  console.log('Here the ingested context data: ', context)
  const s3Config = {
    imageName: event.body
  }
  const childrenCheckinCheckout = new ChildrenCheckinCheckout(s3Config)
  const admissionResponse = await childrenCheckinCheckout.admission()
  console.log('Here the created signed url: ', admissionResponse)
  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin" : "*"
    },
    body: JSON.stringify({
        reception: admissionResponse
    }),
  };
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};